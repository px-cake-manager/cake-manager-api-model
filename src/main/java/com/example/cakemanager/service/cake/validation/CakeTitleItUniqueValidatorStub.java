package com.example.cakemanager.service.cake.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class BridgeCakeTitleIsUniqueValidator implements ConstraintValidator<Unique, String> {
	private final CakeTitleIsUniqueValidator validator;

	public BridgeCakeTitleIsUniqueValidator(CakeTitleIsUniqueValidator validator) {
		this.validator = validator;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return validator.isValid(value, context);
	}
}
