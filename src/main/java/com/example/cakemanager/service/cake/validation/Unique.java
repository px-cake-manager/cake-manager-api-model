package com.example.cakemanager.service.cake.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BridgeCakeTitleIsUniqueValidator.class)
@Target({ElementType.PARAMETER, ElementType.FIELD, ElementType.METHOD,ElementType.ANNOTATION_TYPE, ElementType.RECORD_COMPONENT})
@Retention(RetentionPolicy.RUNTIME)
public @interface Unique {
	String message() default "{cakemanager.cake.title.unique}";
	Class<?>[] groups() default { };
	Class<? extends Payload>[] payload() default { };
}
