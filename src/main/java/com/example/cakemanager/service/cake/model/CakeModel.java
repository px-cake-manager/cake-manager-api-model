package com.example.cakemanager.service.cake.model;

public record CakeModel(String id, String title, String description, String imageUrl) {
}