package com.example.cakemanager.service.cake.model;

import java.util.List;

public record ListOfCakesResponse(List<CakeModel> cakes) {
}
