package com.example.cakemanager.service.cake.validation;

import javax.validation.ConstraintValidator;

public interface CakeTitleIsUniqueValidator extends ConstraintValidator<Unique, String> {
}
