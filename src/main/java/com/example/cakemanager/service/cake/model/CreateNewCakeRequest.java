package com.example.cakemanager.service.cake.model;

import com.example.cakemanager.service.cake.validation.Unique;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;

public record CreateNewCakeRequest(
		@NotBlank @Length(max = 100) @Unique String title, 
		@NotBlank @Length(max = 100) String description, 
		@NotBlank @URL @Length(max = 300) String imageUrl) {
}
